const User = require("../models/User")
const bcrypt = require("bcrypt")
const auth = require("../auth")

const checkEmailExists = (reqBody) => {
  return User.find({email: reqBody.email}).then(result => {
    if (result.length > 0) {
      return 'Email exist'
    } else {
      return 'Email does not exist'
    }
  })
}

// User registration
const registerUser = (reqBody) => {
  let newUser = new User({
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
    is_admin: reqBody.is_admin
  })

  return newUser.save().then((user, error) => {
    if (error) {
      return 'Unexpected Error'
    } else {
      return 'Registration Success'
    }
  })
}

// User authentication
const loginUser = (reqBody) => {
  return User.findOne({email: reqBody.email}).then(result => {
    if (result == null) {
      return false
    } else {
      const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

      if (isPasswordCorrect) {
        return {access: auth.createAccessToken(result)}
      } else {
        return 'User does not exist.'
      }
    }
  })
}

// Retrieve All User Details
const getAllUsers = () => {
  return User.find({is_admin: false}).then(result => {
    return result
  })
}

// Retrieve Single User Details
const getSingleUser = (reqParams) => {
  return User.findById(reqParams.user_id).then(result => {
    return result
  })
}

// Set user as admin (Admin only)
const updateUserAsAdmin = (reqParams) => {
  let updateActiveField = {
    is_admin: true
  }
  return User.findByIdAndUpdate(reqParams.user_id, updateActiveField).then((user, error) => {
    if (error) {
      return 'Unexpected Error. Try again later.'
    } else {
      return 'Your changes have been successfully saved!'
    }
  })
}

module.exports = {
  checkEmailExists,
  registerUser,
  loginUser,
  getAllUsers,
  getSingleUser,
  updateUserAsAdmin
}