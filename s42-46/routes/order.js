const express = require("express")
const router = express.Router()
const orderController = require("../controllers/order")
const auth = require("../auth")

router.post("/checkout", (req, res) => {
  let data = {
    user_id: req.body.user_id,
    product_id: req.body.product_id,
    product_name: req.body.product_name,
    quantity: req.body.quantity,
    total_amount: req.body.total_amount
  }
  orderController.checkout(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router