const express = require("express")
const router = express.Router()
const userController = require("../controllers/user")
const auth = require("../auth")

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body)
		.then(
			result => res.send(result)
		)
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body)
		.then(
			result => res.send(result)
		)
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body)
		.then(
			result => res.send(result)
		)
});

router.get("/retrieveAllUsers", auth.verify, (req, res) => {
	userController.getAllUsers()
		.then(
			result => res.send(result)
		)
})

router.get("/:user_id", auth.verify, (req, res) => {
	userController.getSingleUser(req.params)
		.then(
			result => res.send(result)
		)
})

router.put("/:user_id/activate", auth.verify, (req, res) => {
	userController.updateUserAsAdmin(req.params)
		.then(
			result => res.send(result)
		)
})

module.exports = router